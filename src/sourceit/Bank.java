package sourceit;

public class Bank {
    public String nameBank;
    public float kursBuyUSD;
    public float kursSellUSD;
    public float kursBuyEUR;
    public float kursSellEUR;
    public float kursBuyFunt;
    public float kursSellFunt;

    public Bank(String nameBank, float kursBuyUSD, float kursSellUSD, float kursBuyEUR, float kursSellEUR, float kursBuyFunt, float kursSellFunt) {
        this.nameBank = nameBank;
        this.kursBuyUSD = kursBuyUSD;
        this.kursSellUSD = kursSellUSD;
        this.kursBuyEUR = kursBuyEUR;
        this.kursSellEUR = kursSellEUR;
        this.kursBuyFunt = kursBuyFunt;
        this.kursSellFunt = kursSellFunt;
    }
}
